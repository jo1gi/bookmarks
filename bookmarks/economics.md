# Economics
- Aswath Damodaran](https://www.youtube.com/channel/UCLvnJL8htRR1T9cbSccaoVw)
  Youtube videos from teacher of corporate finance, valuation and investment
  philosophies at the Stern School of Business at New York University
- [CoreEcon](https://www.core-econ.org/)
  Free economics text books
