# NixOS
- [Homepage](https://nixos.org/)
- [How to Learn Nix](https://ianthehenry.com/posts/how-to-learn-nix/)
  Blog posts about how to learn nix (also includes NixOS and expression
  language)

## Similar pages
- [Nix expression languages](../programming_languages/nix.md)
- [Nix package manager](../package_managers/nix.md)
