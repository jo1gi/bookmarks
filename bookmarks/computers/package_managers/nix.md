# Nix package manager
- [How to Learn Nix](https://ianthehenry.com/posts/how-to-learn-nix/)
  Blog posts about how to learn nix (also includes NixOS and expression
  language)

## Similar pages
- [NixOS](../operating_systems/nixos.md)
- [Nix package manager](../package_managers/nix.md)
