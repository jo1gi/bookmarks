# Nix expression language
- [A tour of Nix](https://nixcloud.io/tour/?id=1)
  Interactive guide to nix expression language
- [Nix (builtins) & Nixpkgs (lib) Functions](https://teu5us.github.io/nix-lib.html)
  Builtin functions in nix
- [How to Learn Nix](https://ianthehenry.com/posts/how-to-learn-nix/)
  Blog posts about how to learn nix (also includes NixOS and expression
  language)

## Similar pages
- [Nix package manager](../package_managers/nix.md)
- [NixOS](../operating_systems/nixos.md)
